package info.sheix.arkatris

import com.badlogic.gdx.physics.box2d.Body
import ktx.box2d.body
import ktx.box2d.box

class ObjectHolder {
    private val objectMap = mutableMapOf<String, Body>()
    private var dummyBody: Body? = null

    fun add(id: String, body: Body) {
        objectMap[id] = body
    }

    fun remove(id: String) =
        objectMap.remove(id)

    operator fun get(id: String): Body {
        return objectMap[id] ?: dummyBody ?: createDummyBody()
    }

    private fun createDummyBody(): Body {
        dummyBody = MyGdxGame.context.inject<WorldHolder>().world.body {
            box {  }
        }
        return dummyBody!!
    }

    fun contains(id: String): Boolean {
        return objectMap.containsKey(id)
    }

    fun containsAny(id: String): Boolean =
        objectMap.filter { entry -> entry.key.startsWith(id) }.isNotEmpty().also {
            objectMap.map { println(it.key) }
        }



}
