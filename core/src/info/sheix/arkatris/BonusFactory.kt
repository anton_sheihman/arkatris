package info.sheix.arkatris

import com.badlogic.gdx.math.Vector2
import ktx.box2d.box


class BonusFactory {
    fun createBonus(): BonusAction {
        return EnlargePad()
    }

}

interface BonusAction {
    fun resolve()
}

class EnlargePad : BonusAction {
    override fun resolve() {
        val modifiedObjectsHolder = MyGdxGame.context.inject<ModifiedObjectsHolder>()
        val objectsHolder = MyGdxGame.context.inject<ObjectHolder>()
        val worldHolder = MyGdxGame.context.inject<WorldHolder>()
        modifiedObjectsHolder.addObjectWithAction(objectsHolder[PAD]) {
            println("Bonus action resolving...")

//            it.box(width = 24f, height = 4f, position = Vector2(-5f, 0f)) {
//                density = 1f
//                restitution = 1f
//                friction = 0.2f
//            }
//            println("Added one box, now second")
//
//            it.box(width = 24f, height = 4f, position = Vector2(5f, 0f)) {
//                density = 1f
//                restitution = 1f
//                friction = 0.2f
//            }
        }

    }

}
