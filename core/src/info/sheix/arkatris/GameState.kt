package info.sheix.arkatris

class GameState {

    var level = 1

    fun loseBall() {
        balls -= 1
        checkIfGameOver()
    }

    private fun checkIfGameOver() {
        if (balls < 0) {
            gameOver = true
            level = 1
        }
    }

    fun startNewGame() {
        initWorld()

        gameOver = false
        balls = 3
        score = 0
    }

    fun startNextLevel() {
        level++
        initWorld()
    }

    private fun initWorld() {
        val utility = MyGdxGame.context.inject<WorldUtility>()
        val worldHolder = MyGdxGame.context.inject<WorldHolder>()
        worldHolder.world = utility.initWorld(level)
    }

    internal var gameOver = false
    internal var balls = 3
    internal var score = 0
}
