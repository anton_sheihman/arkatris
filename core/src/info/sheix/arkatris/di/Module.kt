package info.sheix.arkatris.di

import com.badlogic.gdx.assets.AssetManager
import info.sheix.arkatris.*

class Module {
        init {
            MyGdxGame.context.bindSingleton { BonusFactory() }
            MyGdxGame.context.bindSingleton { AssetManager() }
            MyGdxGame.context.bindSingleton { SpriteProvider() }
            MyGdxGame.context.bindSingleton { AssetsLoader() }
            MyGdxGame.context.bindSingleton { WorldHolder() }
            MyGdxGame.context.bindSingleton { ObjectHolder() }
            MyGdxGame.context.bindSingleton { GameState() }
            MyGdxGame.context.bindSingleton { WorldUtility() }
            MyGdxGame.context.bindSingleton { WorldObjectsFactory() }
            MyGdxGame.context.bindSingleton { ModifiedObjectsHolder() }

        }
    }
