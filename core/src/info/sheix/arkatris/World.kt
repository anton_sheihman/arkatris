package info.sheix.arkatris

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.Contact
import com.badlogic.gdx.physics.box2d.World
import ktx.box2d.createWorld
import java.util.*
import kotlin.concurrent.timerTask


data class UserData(val sprite: Sprite, val id: String, val special: Any? = null)



class WorldUtility {
    private lateinit var timeToLaunchPad: Timer

    fun dropAndCreateBall(world: World) {
        val ballBody = MyGdxGame.context.inject<ObjectHolder>()[BALL]
        world.destroyBody(ballBody)
        MyGdxGame.context.inject<WorldObjectsFactory>().createBall(world)
    }

    fun dropAndCreatePad(world: World) {
        val padBody = MyGdxGame.context.inject<ObjectHolder>()[PAD]
        world.destroyBody(padBody)

        timeToLaunchPad = Timer()

        timeToLaunchPad.schedule(timerTask { MyGdxGame.context.inject<WorldObjectsFactory>().createPad(world) }, 1000L)

    }

    fun initWorld(level: Int): World {

        val worldObjectsFactory = MyGdxGame.context.inject<WorldObjectsFactory>()

        val gravity = Vector2(0f, -9.8f)
        val myWorld = createWorld(gravity, false)

        worldObjectsFactory.createPad(myWorld)
        worldObjectsFactory.createBall(myWorld)
        worldObjectsFactory.createGround(myWorld)
        worldObjectsFactory.createSky(myWorld)

        worldObjectsFactory.createWalls(myWorld)

        worldObjectsFactory.createBricks(myWorld, level)

        myWorld.setContactListener(ObjectContactListener())
        return myWorld
    }

}

class WorldHolder {
    lateinit var world: World
}


enum class EntityType {
    PAD,
    BALL,
    GROUND,
    SMALL_GROUND,
    BRICK,
    BONUS
}


fun Contact.getBody(id: String): Body = when {
    (this.fixtureA.body.userData as UserData).id.startsWith(id) -> fixtureA.body
    (this.fixtureB.body.userData as UserData).id.startsWith(id) -> fixtureB.body
    else -> throw NotFoundError()
}

fun Contact.isContactOf(first: String, second: String) =
    ((isFirstBody(this, first) && isSecondBody(this, second))
            || (isFirstBody(this, second) && isSecondBody(this, first)))

private fun isFirstBody(contact: Contact, id: String) =
    (contact.fixtureA.body.userData as UserData).id.startsWith(id)

private fun isSecondBody(contact: Contact, id: String) =
    (contact.fixtureB.body.userData as UserData).id.startsWith(id)

const val BALL = "ball"
const val BRICK = "brick"
const val PAD = "pad"
const val BONUS = "bonus"

