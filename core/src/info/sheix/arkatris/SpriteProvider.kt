package info.sheix.arkatris

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import ktx.freetype.loadFreeTypeFont
import ktx.freetype.registerFreeTypeFontLoaders

class SpriteProvider {
    private val assetManager: AssetManager = MyGdxGame.context.inject()


    fun getEntity(Type: EntityType): Sprite {
        return when (Type) {
            EntityType.PAD -> Sprite(assetManager.get<Texture>(padImage))
            EntityType.BALL -> Sprite(assetManager.get<Texture>(ballImage))
            EntityType.GROUND -> Sprite(assetManager.get<Texture>(groundImage))
            EntityType.SMALL_GROUND -> Sprite(assetManager.get<Texture>(smallGroundImage))
            EntityType.BRICK -> Sprite(assetManager.get<Texture>(brickImage))
            EntityType.BONUS -> Sprite(assetManager.get<Texture>(bonusImage))
        }
    }

}


class AssetsLoader {

    fun loadAssets() {
        val assetManager = MyGdxGame.context.inject<AssetManager>()

        assetManager.registerFreeTypeFontLoaders()
        assetManager.loadFreeTypeFont(fontFileName)


        assetManager.load(padImage, Texture::class.java)
        assetManager.load(ballImage, Texture::class.java)
        assetManager.load(groundImage, Texture::class.java)
        assetManager.load(smallGroundImage, Texture::class.java)
        assetManager.load(brickImage, Texture::class.java)
        assetManager.load(bonusImage, Texture::class.java)

        assetManager.finishLoading()
    }

}

val padImage = "images/pad.png"
val ballImage = "images/ball.png"
val groundImage = "images/ground.png"
val smallGroundImage = "images/small_ground.png"
val brickImage = "images/brick.png"
val bonusImage = "images/bonus.png"

val fontFileName = "fonts/Px437_IBM_ISO9.ttf"
