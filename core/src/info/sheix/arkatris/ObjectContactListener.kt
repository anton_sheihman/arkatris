package info.sheix.arkatris

import com.badlogic.gdx.physics.box2d.Contact
import com.badlogic.gdx.physics.box2d.ContactImpulse
import com.badlogic.gdx.physics.box2d.ContactListener
import com.badlogic.gdx.physics.box2d.Manifold

class ObjectContactListener : ContactListener {
    private val objectsToDestroy = MyGdxGame.context.inject<ModifiedObjectsHolder>()
    private val objects = MyGdxGame.context.inject<ObjectHolder>()
    private val gameState = MyGdxGame.context.inject<GameState>()
    private val worldObjectsFactory = MyGdxGame.context.inject<WorldObjectsFactory>()
    private val worldHolder = MyGdxGame.context.inject<WorldHolder>()

    override fun endContact(contact: Contact) {
        if (contact.isContactOf(BALL, BRICK)) {
            processBallBrick(contact)
        }
        if (contact.isContactOf(PAD, BONUS)) {
            processPadBonus(contact)
        }

    }

    private fun processPadBonus(contact: Contact) {
        val userData = contact.getBody(BONUS).userData as UserData
        println("contact with ${userData.id}")
        val body = objects[userData.id]

        if (userData.special is BonusAction) {
            userData.special.resolve()
        }

        objects.remove(userData.id)
        objectsToDestroy.addObjectWithAction(body) {
            worldHolder.world.destroyBody(it)
        }



    }

    private fun processBallBrick(contact: Contact) {
        val userData = contact.getBody(BRICK).userData as UserData
        val brickId = userData.id
        val body = objects[brickId]
        gameState.score += 10

        if (userData.special != null) {
            objectsToDestroy.addObjectWithAction(body) {
                worldObjectsFactory.createBonus(
                    worldHolder.world,
                    body.position
                )
            }
        }

        objectsToDestroy.addObjectWithAction(body) { worldHolder.world.destroyBody(it) }
        objects.remove(brickId)

        if (!objects.containsAny(BRICK)) {
            println("NO MORE BRICKS NEXT LEVEL")
            val gameState = MyGdxGame.context.inject<GameState>()
            gameState.startNextLevel()
        }
    }

    override fun beginContact(contact: Contact) {

    }


    override fun preSolve(contact: Contact?, oldManifold: Manifold?) {

    }

    override fun postSolve(contact: Contact?, impulse: ContactImpulse?) {

    }


}