package info.sheix.arkatris

import com.badlogic.gdx.Input
import com.badlogic.gdx.math.Vector2
import ktx.app.KtxInputAdapter
import kotlin.system.exitProcess

class MyKtxInputAdapter : KtxInputAdapter {

    private val objects = MyGdxGame.context.inject<ObjectHolder>()

    override fun scrolled(amountX: Float, amountY: Float): Boolean {
        TODO("Not yet implemented")
    }

    override fun keyUp(keycode: Int): Boolean {
        println("keycode : $keycode")
        val padBody = objects["pad"]
        when (keycode) {

            Input.Keys.LEFT -> objects["pad"].applyForceToCenter(Vector2(-100_000_000f, 0f), false)
            Input.Keys.RIGHT -> objects["pad"].applyForceToCenter(Vector2(100_000_000f, 0f), false)
//            Input.Keys.LEFT -> padBody.linearVelocity = padBody.linearVelocity.add(-100f, 50*padBody.angle)
//            Input.Keys.RIGHT -> padBody.linearVelocity = padBody.linearVelocity.add(100f, -padBody.angle*50)
            Input.Keys.UP -> padBody.angularVelocity = padBody.angularVelocity.plus(0.75f)
            Input.Keys.ESCAPE ->
                exitProcess(0)

        }


        return super.keyUp(keycode)
    }

}

class GameOverInputAdapter : KtxInputAdapter {
    override fun scrolled(amountX: Float, amountY: Float): Boolean {
        TODO("Not yet implemented")
    }
    private val gameState = MyGdxGame.context.inject<GameState>()

    override fun keyUp(keycode: Int): Boolean {
        println("keycode : $keycode")
        when (keycode) {
            Input.Keys.ESCAPE ->
                exitProcess(0)
            Input.Keys.SPACE -> {
                gameState.startNewGame()

            }
        }


        return super.keyUp(keycode)
    }
}
