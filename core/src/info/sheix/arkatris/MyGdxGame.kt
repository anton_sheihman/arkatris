package info.sheix.arkatris

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.utils.Array
import info.sheix.arkatris.di.Module
import ktx.assets.getAsset
import ktx.graphics.use
import ktx.inject.Context


class MyGdxGame : ApplicationAdapter() {
    companion object GameContext {
        val context = Context()
        val module = Module()

    }

    private lateinit var inputListener: InputProcessor
    private lateinit var gameOverInputAdapter: InputProcessor
    private val objects = context.inject<ObjectHolder>()
    private val worldHolder = context.inject<WorldHolder>()
    private lateinit var batch: SpriteBatch
    private val camera: OrthographicCamera = OrthographicCamera()

    lateinit var debugRenderer: Box2DDebugRenderer

    override fun create() {
        camera.setToOrtho(false, 200f, 120f);
        val loader = context.inject<AssetsLoader>()
        loader.loadAssets()

        batch = SpriteBatch()
        val gameState = context.inject<GameState>()
        gameState.startNewGame()

        inputListener = MyKtxInputAdapter()
        gameOverInputAdapter = GameOverInputAdapter()
        debugRenderer = Box2DDebugRenderer()
    }


    override fun render() {
        Gdx.gl.glClearColor(1f, 1f, 1f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        val bodies: Array<Body> = Array()
        val world = worldHolder.world
        debugRenderer.render(world, camera.combined)
        batch.projectionMatrix = camera.combined
        val gameState = context.inject<GameState>()
        batch.use { b ->
            if (!gameState.gameOver) {
                Gdx.input.inputProcessor = inputListener
                renderBodies(b, world, bodies)
                renderState(b, gameState)
            } else {
                Gdx.input.inputProcessor = gameOverInputAdapter
                renderGameOver(b, gameState)
            }
        }

        world.step(1 / 30f, 12, 6)
        destroyUnneededBodies()
        processStateChanges(world)
    }

    private fun renderBodies(
        b: SpriteBatch,
        world: World,
        bodies: Array<Body>
    ) {
        world.getBodies(bodies)
        bodies.map { body ->
            if (body.userData != null) {
                with((body.userData as UserData).sprite) {
                    this.draw(b)
                    this.setPosition(body.position.add(Vector2(-this.originX, -this.originY)))
                    this.setScale(0.25f)
                    this.rotation = MathUtils.radiansToDegrees * body.angle;
                }
            }
        }
    }

    private fun renderGameOver(batch: SpriteBatch, gameState: GameState) {
        val assetManager = context.inject<AssetManager>()
        val asset = assetManager.getAsset<BitmapFont>(fontFileName)
        asset.color = Color.BLACK
        asset.draw(batch, "GAME OVER", 20f, 80f)
        asset.draw(batch, "YOUR SCORE: ${gameState.score}", 20f, 68f)
        asset.data.scale(-0.3f)
        asset.draw(batch, "SPACE FOR NEW GAME", 20f, 55f)
        asset.draw(batch, "ESC FOR EXIT", 20f, 45f)
        asset.data.scale(+0.3f)
    }

    private fun renderState(batch: SpriteBatch, gameState: GameState) {

        val assetManager = context.inject<AssetManager>()
        val asset = assetManager.getAsset<BitmapFont>(fontFileName)
        asset.color = Color.BLACK
        asset.data.scale(-0.3f)
        asset.draw(batch, "Balls: ${gameState.balls}  Score: ${gameState.score}", 20f, 115f)
        asset.data.scale(+0.3f)
    }

    private fun destroyUnneededBodies() {
        val objectsToActOn = context.inject<ModifiedObjectsHolder>()
        objectsToActOn.drainObjectsToActOn().forEach { (body, action) ->
            if (!worldHolder.world.isLocked)
                action.invoke(body)
        }
    }

    private fun processStateChanges(world: World) {
        val gameState = context.inject<GameState>()
        if (objectFell(BALL)) {
            println("ball down!")
            gameState.loseBall()
            context.inject<WorldUtility>().dropAndCreateBall(world)
        }
        if (objects.contains(PAD)) {
            if (objectFell(PAD)) {
                println("pad down!")
                println("time penalty!")
                context.inject<WorldUtility>().dropAndCreatePad(world)
                objects.remove("pad")
            }
        }
    }

    private fun objectFell(kind: String) = objects[kind].position.y < 5f

    override fun dispose() {
        batch.dispose()
    }
}


fun Sprite.setPosition(vector: Vector2) {
    this.setPosition(vector.x, vector.y)
}


