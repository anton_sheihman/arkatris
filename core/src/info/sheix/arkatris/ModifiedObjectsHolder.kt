package info.sheix.arkatris

import com.badlogic.gdx.physics.box2d.Body
import java.util.concurrent.ArrayBlockingQueue


class ModifiedObjectsHolder {
    private val bodiesToAct = ArrayBlockingQueue<Pair<Body, (Body) -> Unit>>(100)

    fun addObjectWithAction(body: Body, action: (Body) -> Unit) {
        bodiesToAct.add(Pair(body, action))
    }

    fun drainObjectsToActOn(): List<Pair<Body, (Body)-> Unit >> {
        val list = mutableListOf<Pair<Body, (Body)->Unit >>()
        bodiesToAct.drainTo(list)
        return list
    }

}