package info.sheix.arkatris

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.World
import info.sheix.arkatris.entities.Bonus
import ktx.box2d.BodyDefinition
import ktx.box2d.body
import ktx.box2d.box
import ktx.box2d.circle

class WorldObjectsFactory {
    private var bonusNumber = 0

    private var objectHolder: ObjectHolder = MyGdxGame.context.inject()
    private var spriteProvider: SpriteProvider = MyGdxGame.context.inject()
    private var bonusFactory: BonusFactory = MyGdxGame.context.inject()

    fun createWalls(
        myWorld: World
    ) {

        val leftWallPosition = Vector2(10f, 50f)
        val leftWall = myWorld.body {
            type = BodyDef.BodyType.StaticBody
            position.set(leftWallPosition)
            angle = Math.PI.toFloat() / 2f

            wallFixture()
        }
        val wallSprite = spriteProvider.getEntity(EntityType.GROUND)
        leftWall.userData = UserData(wallSprite, "leftWall")
        objectHolder.add("leftWall", leftWall)

        val rightWallPosition = Vector2(190f, 50f)
        val rightWall = myWorld.body {
            type = BodyDef.BodyType.StaticBody
            position.set(rightWallPosition)
            angle = Math.PI.toFloat() / 2f

            wallFixture()
        }
        val wallSpriteRight = spriteProvider.getEntity(EntityType.GROUND)
        rightWall.userData = UserData(wallSpriteRight, "rightWall")
        objectHolder.add("rightWall", rightWall)
    }

    private fun BodyDefinition.wallFixture() {
        box(width = 100f, height = 2f) {
            density = 1f
            friction = 0.0f
            restitution = 1f
        }
    }

    fun createPad(
        myWorld: World
    ) {
        val initialPadPosition = Vector2(100f, 30f)

        val pad = createPadAtPosition(myWorld, initialPadPosition)
        objectHolder.add("pad", pad)
    }

    private fun createPadAtPosition(myWorld: World, initialPadPosition: Vector2): Body {
        val pad = myWorld.body {
            type = BodyDef.BodyType.DynamicBody
            position.set(initialPadPosition)

            box(width = 24f, height = 4f) {
                density = 1.7f
                restitution = 0.5f
                friction = 0.2f
            }
        }
        pad.linearDamping = 1.3f
        pad.angularDamping = 1.3f
        pad.userData = UserData(spriteProvider.getEntity(EntityType.PAD), "pad")
        return pad
    }


    fun createBonus(
        myWorld: World, initialBallPosition: Vector2
    ) {
        val bonus = myWorld.body {
            type = BodyDef.BodyType.DynamicBody
            position.set(initialBallPosition)
            box(width = 4f, height = 2f) {
                density = 1f
                friction = 0.0f
                restitution = 2f
            }
        }

        bonus.userData = UserData(spriteProvider.getEntity(EntityType.BONUS), "${BONUS}_$bonusNumber", bonusFactory.createBonus())
        bonusNumber += 1
        objectHolder.add(BONUS, bonus)
    }

    fun createBall(
        myWorld: World, initialBallPosition: Vector2 = Vector2(150f, 100f)
    ) {
        val ball = myWorld.body {
            type = BodyDef.BodyType.DynamicBody
            position.set(initialBallPosition)
            circle(radius = 2f) {
                density = 0.8f
                friction = 2f
                restitution = 1.5f
            }
        }

        ball.userData = UserData(spriteProvider.getEntity(EntityType.BALL), BALL)
        objectHolder.add(BALL, ball)
    }

    fun createGround(
        myWorld: World
    ) {
        (0..5).map {
            val groundPosition = Vector2(it * 40f, 10f)
            val ground = myWorld.body {
                type = BodyDef.BodyType.StaticBody
                position.set(groundPosition)
                box(width = 27.5f, height = 2f) {
                    density = 1f
                    friction = 0.0f
                    restitution = 1f
                }
            }
            ground.userData = UserData(spriteProvider.getEntity(EntityType.SMALL_GROUND), "ground")
            objectHolder.add("small_ground($it)", ground)
        }

    }

    fun createSky(
        myWorld: World
    ) {
        val skyPosition = Vector2(100f, 100f)
        val sky = myWorld.body {
            type = BodyDef.BodyType.StaticBody
            position.set(skyPosition)
            box(width = 200f, height = 2f) {
                density = 1f
                friction = 0.0f
                restitution = 1f
            }
        }
        sky.userData = UserData(spriteProvider.getEntity(EntityType.GROUND), "sky")
        objectHolder.add("sky", sky)
    }


    fun createBricks(
        myWorld: World,
        level: Int
    ) {
        val numberOfBricks = 20
        (1..level).map { y ->
            (1..numberOfBricks).map { x ->
                val brickPosition = Vector2(185f - x * 8, y * 4 + 80f)
                val brick = myWorld.body {
                    type = BodyDef.BodyType.StaticBody
                    position.set(brickPosition)
                    box(width = 4f, height = 2f) {
                        density = 1f
                        friction = 0.0f
                        restitution = 2f
                    }
                }
                brick.userData = UserData(spriteProvider.getEntity(EntityType.BRICK), "brick_${x}_${y}", Bonus())
                objectHolder.add("brick_${x}_${y}", brick)
            }
        }
    }
}
